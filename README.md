# wrappers

This repository contains the wrapers created for retrieving configuration for DQM team.

## Run T0 wrapper
```
bash recoWrapper.sh -j CONFIG_FILE FILE_PATH
```

where FILE_PATH is the absolute file path in the format `file:/path/to/file` (e.g. `file:/eos/cms/tier0/store/data/Run2024C/BTagMu/RAW/v1/000/379/415/00000/c1fe5b9f-243d-4358-af3c-a85adb28f2bd.root`).

## Run on BTagMu dataset
In order to test on a specific dataset, one has to define a custom config file to pass to `recoWrapper.sh`.
There are some examples in `config/`.
For example, to run on a file of the BTagMu 2024 dataset and run all the sequences needed to harvest the BTV offline DQM histograms one can run:
```
bash recoWrapper.sh -j config/config_BTagMu_btag_miniAODDQM.json file:/eos/cms/tier0/store/data/Run2024C/BTagMu/RAW/v1/000/379/415/00000/c1fe5b9f-243d-4358-af3c-a85adb28f2bd.root
```

